# coding=utf-8
import time, os, unittest
from HTMLTestRunner import HTMLTestRunner


class TestRunner(object):
    ''' Run test '''

    def __init__(self, cases="./case/", title=u'心之力医生端WEB', description=u'测试环境WEB'):
        self.cases = cases
        self.title = title
        self.des = description

    def run(self):

        # for filename in os.listdir(self.cases):
        #     if filename == "report":
        #         break
        # else:
        #     os.mkdir(self.cases + '/report')
        case_dir = './case/'
        now = time.strftime("%Y-%m-%d_%H_%M_%S")
        fp = open("./report/" + now + "result.html", 'wb')
        tests = unittest.defaultTestLoader.discover(case_dir, pattern='test_*.py',top_level_dir=None)
        runner = HTMLTestRunner(stream=fp, title=self.title, description=self.des)
        runner.run(tests)
        fp.close()

    def debug(self):
        tests = unittest.defaultTestLoader.discover(self.cases, pattern='text_*.py', top_level_dir=None)
        runner = unittest.TextTestRunner()
        runner.run(tests)

#程序执行入口
if __name__ == '__main__':
    test = TestRunner()
    test.run()

