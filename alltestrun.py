#-*- coding:utf-8 -*-
import unittest
import datetime
import time,os
import  HTMLTestRunner
case_path=os.path.join(os.getcwd())
report_path=os.path.join(os.getcwd())
print(report_path)
def all_case():
    discover=unittest.defaultTestLoader.discover(case_path, pattern='test_*.py', top_level_dir=None)
    print(discover)
    return discover
if __name__=='__main__':
   nowtime=time.strftime('%Y-%m-%d  %H_%M_%S',time.localtime(time.time()))
   report_url=(report_path+nowtime+"result.html")
   fp=open(report_url,"wb")
   runner=HTMLTestRunner.HTMLTestRunner(stream=fp,title='XZL WEB UI TEST',description=u'用例执行情况')
   runner.run(all_case())
   fp.close()
# def all_case():
#     case_dir='./case/'
#     suite=unittest.TestSuite()
#     discover=unittest.defaultTestLoader.discover(case_dir, pattern='test_*.py', top_level_dir=None)
#     suite.addTests(discover)
# if __name__=="__main__":
#     report_dir=r"./report/"
#     nowtime=time.strftime('%Y-%m-%d  %H_%M_%S')
#     report_name=(report_dir+'/'+nowtime+'result.html')
#     fp=open(report_name,"wb")
#     report=HTMLTestRunner(stream=fp,title='XZL WEB UI TEST',description=u'用例执行情况')
#     report.run(all_case())
#     fp.close()